# My_MDS2_DMS_project

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.gwdg.de/anael.ngome/my_mds2_dms_project.git
git branch -M main
git push -uf origin main
```

## Name
My_MDS2_DMS_project

## Description
This is my project of my Databank for MDS2 documents (Manufacturer diclosur statement for the security of medical devices) that I have developped during a working student job. 

The project contains an Access Folder to have access to the databank and an Intsructions folder for the user in german language.

## Installation
To use the databank you will need to download Microsoft Access 2007

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
By any Issues you can search for help on the Microsoft page.

## Authors and acknowledgment
I thanked Sana Medizintechnisches Servicezentrum and my IT-Sicherheit Team for helping me and support me in this project with their good feebacks, ideas and appreciations of my work.

## Project status
The project is still in development
